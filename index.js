const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const connection = require('./database/database');
const Pergunta = require('./database/Pergunta');
const Resposta = require('./database/Resposta');

//database
connection.authenticate()
.then(() => {
    console.log('Conexão com banco de dados realizada com sucesso!');
}).catch((msgErro) => {
    console.log('Erro ao se conectar com o banco de dados: ' + msgErro);
});

//informando para o express q usará o ejs como view engine
app.set('view engine', 'ejs');
app.use(express.static('public'));

//bodyparser para receber dados do front
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

//rotas
//exibir dados do banco na página
//'raw: true' é para pegar do banco apenas os dados relevantes no banco de dados
app.get('/', (req, res) => {
    Pergunta.findAll({raw: true, order:[
        ['id', 'DESC'] //DESC = decrescente, ASC = crescente
    ]}).then(perguntas => {
        res.render("index", {
            perguntas: perguntas
        });
    });
    
});

app.get('/perguntar', (req, res) => {
    res.render('perguntar');
});

//salvando dados do front no banco de dados
app.post('/salvarpergunta', (req, res) => {
    var titulo = req.body.titulo;
    var descricao = req.body.descricao;
    Pergunta.create({
        titulo: titulo,
        descricao: descricao
    }).then(() => {
        res.redirect('/');
    })
});

//exibir a pergunta por completo
app.get('/pergunta/:id', (req, res) => {
    var id = req.params.id;
    //procurar ela no banco de dados
    Pergunta.findOne({
        where: {id: id},
    }).then(pergunta => {
        //se encontrar, retornará ela, caso contrário, retornara um dado undefined
        if(pergunta != undefined){

            Resposta.findAll({
                where: {perguntaId: pergunta.id},
                order: [
                    ['id', 'DESC']
                ]
            }).then(respostas => {
                //render joga para uma view dentro do projeto
                res.render('pergunta', {
                //jogar a variavel pergunta, q contém a pergunta q pegou do banco, para a view 'pergunta'
                pergunta: pergunta,
                respostas: respostas
                });
            });

            
        }else{
            //redirect joga para um link
            res.redirect('/');
        }
    });
});

app.post('/responder', (req, res) => {
    var corpo = req.body.corpo;
    var perguntaId = req.body.pergunta;

    Resposta.create({
        corpo: corpo,
        perguntaId: perguntaId
    }).then(() => {
        res.redirect('/pergunta/'+perguntaId);
    })
});

//porta do servidor
app.listen(8080, () => {
    console.log('Aplicação rodando');
});